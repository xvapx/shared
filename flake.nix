{
  description = "Shared configuration for marti's managed flakes";

  inputs = rec {
    # Use unstable nixpkgs by default, flake users can override this
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:nixos/nixpkgs/master";
    # Home-manager, using the same nixpkgs
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Dotfiles, also with the same nixpkgs
    dotfiles-marti = {
      url = "gitlab:xvapx/dotfiles?ref=development";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, nixpkgs-master, home-manager, dotfiles-marti }:
  let
    # Architectures and systems supported by this flake
    supported_systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];
  in rec {
      # Re-export nixpkgs, home-manager & dotfiles-marti for flake users
      inherit nixpkgs nixpkgs-master home-manager dotfiles-marti;
      # Add our functions to lib and export it too
      lib = nixpkgs.lib // import ./shared_lib.nix { inherit (nixpkgs) lib; };

      # Export dotfiles, users & groups
      _dotfiles = { pkgs }:
        # Use my dotfiles without the module to allow consumer flakes to use it instead.
        { marti = dotfiles-marti.dotfiles { inherit pkgs; }; };
      _users = { pkgs }: import ./users.nix {
        inherit lib pkgs;
        dotfiles = _dotfiles { inherit pkgs; };
      };
      groups = import ./groups.nix;

      # Export common network settings
      network = import ./network.nix;

      # Export overlay with custom packages
      overlays.default = final: prev: {
        assistance = prev.callPackage ./packages/assistance { };
        remmina = prev.remmina.overrideAttrs (old: rec { # remmina issue https://gitlab.com/Remmina/Remmina/-/issues/3207
          src = prev.pkgs.fetchFromGitLab {
            owner = "Remmina";
            repo = "Remmina";
            rev = "rev = v1.4.35";
            hash = "sha256-u+ysAFi7I7nXIiAw7VCmHbqgtRoZgkPnRfy/Mnl1b2g=";
          };
        });
        ssh-moduli = prev.callPackage ./packages/ssh-moduli { };
        clamav = prev.clamav.overrideAttrs (old: rec { # https://github.com/NixOS/nixpkgs/issues/334852
          version = "1.4.0";
          src = prev.pkgs.fetchurl {
            url = "https://www.clamav.net/downloads/production/${old.pname}-${version}.tar.gz";
            hash = "sha256-1nqymeXKBdrT2imaXqc9YCCTcqW+zX8TuaM8KQM4pOY=";
          };
        });
        amule-gui = prev.amule-gui.override {
          boost = prev.boost186;
        };
      };

      # Export module with roles, profiles, home-manager
      nixosModules = {
        module = {
          imports = [
            # roles & profiles
            ./module.nix
            # home-manager
            home-manager.nixosModule
            # overlays
            ({ nixpkgs.overlays = [ overlays.default ]; })
            # enable unfree packages
            ({ nixpkgs.config.allowUnfree = true; })
          ];
        };
      };
      nixosModule = self.nixosModules.module;

      # Custom system fot the installer ISOs
      nixosConfigurations = let
        installerFor = system: nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            (import ./installer.nix)
            nixosModule
            dotfiles-marti.nixosModule
            "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
            groups
            (_users { pkgs = import nixpkgs {
              inherit system;
              config.allowUnfree = true;
            }; })
          ];
        };
      # Generate a nixosConfiguration for each supported system
      in builtins.listToAttrs (map (system: {
        name = "installer-${system}";
        value = (installerFor system);
      }) supported_systems);

      # Installer ISOS, build with `nix build .#isos.installer-x86_64-linux`, change the system if desired.
      isos = builtins.listToAttrs (map (system: {
        name = "installer-${system}";
        value = nixosConfigurations."installer-${system}".config.system.build.isoImage;
      }) supported_systems);

      # Devshells for nix develop, for all supported systems
      devShells = builtins.listToAttrs ( map ( system: {
        name = "${system}";
        value = { default = import ./shell.nix { pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        }; }; };
      }) supported_systems);

      # Hydra builds
      hydraJobs = {
        ISOs = {
          x86_64 = isos.installer-x86_64-linux;
        };
      };

    };

}

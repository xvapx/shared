{
  nameservers = {

    google = {
      ipv4 = [
        "8.8.8.8"
        "8.8.4.4"
      ];
      ipv6 = [
        "2001:4860:4860::8888"
        "2001:4860:4860::8844"
      ];
    };

    opendns-familyShield = {
      ipv4 = [
        "208.67.222.123"
        "208.67.220.123"
      ];
      ipv6 = [
        "2620:119:35::123"
        "2620:119:53::123"
      ];
    };

    opendns-home = {
      ipv4 = [
        "208.67.222.222"
        "208.67.220.220"
      ];
      ipv6 = [
        "2620:119:35::35"
        "2620:119:53::53"
      ];
    };

    cloudflare = {
      ipv4 = [
        "1.1.1.1"
        "1.0.0.1"
      ];
      ipv6 = [
        "2606:4700:4700::1111"
        "2606:4700:4700::1001"
      ];
      tor = [
        "https://dns4torpnlfs2ifuz2s2yf3fc7rdmsbhm6rw75euj35pac6ap25zgqad.onion/"
      ];
    };

  };

  nixosBinaryCaches = [
    https://cache.nixos.org
    https://hydra.nixos.org
  ];

  binaryCachePublicKeys = [
    "hydra.nixos.org-1:CNHJZBh9K4tP3EKF6FkkgeVYsS3ohTl+oS0Qa8bezVs="
  ];
}

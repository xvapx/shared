{ pkgs, lib, ... }:

{

  # Disable profiles, roles & user modules for the installer
  marti = {
    profiles.enable = false;
    roles.enable = false;
    dotfiles.enable = false;
  };

  # Use the latest Linux kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;
    
  # enable shell on boot fail
  boot.kernelParams = [ "boot.shell_on_fail" ];

  # Support all filesystems except zfs for boot
  boot.supportedFilesystems = lib.mkForce [ "btrfs" "reiserfs" "vfat" "f2fs" "xfs" "ntfs" "cifs" ];

  # Packages to make available in the live system
  environment.systemPackages = with pkgs; [
    # Basic tools
    dmidecode
    elinks
    file
    git
    gnupg
    htop
    lm_sensors
    lshw
    man-pages man-pages-posix
    mc
    nano
    neovim
    p7zip
    pass
    patchelf
    pciutils
    progress
    psmisc
    rsync
    rclone
    sdparm
    smartmontools
    strace
    tmux
    tree
    unar
    units
    unshield
    usbutils
    wget
    # Downloads
    aria2
    # Disks 
    cifs-utils
    cdrkit
    hddtemp
    hdparm
    parted
    partimage
    exfat exfatprogs
    ntfs3g
    # Forensics
    ddrescue
    diskscan
    testdisk
    # Networking
    httpie
    inetutils
    iperf
    nmap
    # Nix tools
    nix-index
    nix-du
    nix-prefetch-scripts
    nox

  ];

  # Enable nix flakes
  nix = {
    package = pkgs.nixVersions.latest;
    extraOptions = "experimental-features = nix-command flakes";
  };

  programs = {
    bash.completion.enable = true;
    tmux = import ./roles/common/tmux_config.nix { inherit pkgs; };
    less = {
      enable = true;
    };
  };

  services = {
    # Enable ssh
    openssh.enable = true;
  };

  networking = {
    # Enable DCHP for all network interfaces
    useDHCP = true;
  };


  isoImage = {
    # Copy repositories to ISO image
    contents = [
      # format: https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/installer/cd-dvd/iso-image.nix
    ];
    # Include all the sources to build the system in the image.
    includeSystemBuildDependencies = true;
    # Make the image bootable
    makeEfiBootable = true;
    makeUsbBootable = true;
  };

}

{ config, lib, options, pkgs, ... }:

with lib;

let

  profiles = import ./profiles.nix {
    inherit lib pkgs;
    cfg = config.marti.profiles;
  };

  roles = import ./roles.nix {
    inherit lib pkgs;
    cfg = config.marti.roles // {
      profiles = config.marti.profiles;
      hardware = config.hardware;
      ids = config.ids;
      fonts = config.fonts;
    };
    secrets = config.age.secrets or {};
  };

in

{

  imports = [
    profiles
    roles
  ];

}

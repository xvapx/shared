{ cfg, lib, pkgs }:

with lib;

{

  options.marti.profiles.enable = mkOption {
    type = types.bool;
    default = true;
    description = "Wether to enable marti's shared hardware profiles.";
  };

  config = mkMerge [
    (mkIf cfg.enable {
      # Enable all redistributable firmware.
      hardware.enableRedistributableFirmware = true;
    })
    (mkIf (!cfg.enable) {
      marti.profiles.cpu.enable = mkForce false;
      marti.profiles.gpu.enable = mkForce false;
      marti.profiles.network.enable = mkForce false;
      marti.profiles.filesystems.enable = mkForce false;
      marti.profiles.yubikey.enable = mkForce false;
    })
  ];

  imports = [
    (import ./profiles/cpu.nix { inherit cfg lib pkgs; })
    (import ./profiles/gpu.nix { inherit cfg lib pkgs; })
    (import ./profiles/network.nix { inherit cfg lib pkgs; })
    (import ./profiles/filesystems.nix { inherit cfg lib pkgs; })
    (import ./profiles/yubikey.nix { inherit cfg lib pkgs; })
  ];

}

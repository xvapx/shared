# marti shared users
{ lib, pkgs, dotfiles }:

let

  system_defaults = {
    home-manager = {
      # install user packages to /etc/profiles
      useUserPackages = true;
      # use the system nixpkgs
      useGlobalPkgs = true;
    };
  };

in

lib.recursive_merge [
  (import ./users/marti_admin.nix { inherit pkgs dotfiles; })
  system_defaults
]

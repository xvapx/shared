{ cfg, lib, pkgs, secrets, ... }:

with lib;

{

  options.marti.roles.enable = mkOption {
    type = types.bool;
    default = true;
    description = "Wether to enable marti's shared roles.";
  };

  config = mkIf (!cfg.enable) {
    marti.roles.common.enable = mkForce false;
    marti.roles.desktop.enable = mkForce false;
    marti.roles.developer.enable = mkForce false;
    marti.roles.editor.enable = mkForce false;
    marti.roles.flatpak.enable = mkForce false;
    marti.roles.gaming.enable = mkForce false;
    marti.roles.server.enable = mkForce false;
    marti.roles.social.enable = mkForce false;
    marti.roles.virtualization.enable = mkForce false;
    marti.roles.vpn.enable = mkForce false;
  };

  imports = [
    (import ./roles/common.nix { inherit cfg lib pkgs; })
    (import ./roles/desktop.nix { inherit cfg lib pkgs; })
    (import ./roles/developer.nix { inherit cfg lib pkgs; })
    (import ./roles/editor.nix { inherit cfg lib pkgs; })
    (import ./roles/flatpak.nix { inherit cfg lib pkgs; })
    (import ./roles/gaming.nix { inherit cfg lib pkgs; })
    (import ./roles/server.nix { inherit cfg lib pkgs; })
    (import ./roles/social.nix { inherit cfg lib pkgs; })
    (import ./roles/virtualization.nix { inherit cfg lib pkgs; })
    (import ./roles/vpn.nix { inherit cfg lib pkgs secrets; })
  ];

}

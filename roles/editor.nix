{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.editor.enable = mkOption {
    type = types.bool;
    default = true;
    description = "Wether to enable marti's shared editor roles.";
  };

  config = mkIf (!cfg.editor.enable) {
    marti.roles.editor.cad.enable = mkForce false;
    marti.roles.editor.document.enable = mkForce false;
    marti.roles.editor.image.enable = mkForce false;
    marti.roles.editor.multimedia.enable = mkForce false;
  };

  imports = [
    (import ./editor/cad.nix { inherit cfg lib pkgs; })
    (import ./editor/document.nix { inherit cfg lib pkgs; })
    (import ./editor/image.nix { inherit cfg lib pkgs; })
    (import ./editor/multimedia.nix { inherit cfg lib pkgs; })
  ];

}

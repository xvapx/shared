{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.developer = {
    enable = mkEnableOption "Wether to enable developer software.";
  };

  config = mkMerge [
    (mkIf cfg.developer.enable {
      environment.systemPackages =
        with pkgs; [
        # Editors
        emacs
        helix
        kakoune
        neovim
        vim
        # Build tools.
        automake
        binutils
        cmake
        gnumake
        pkg-config
        # Compilers.
        gcc
        clang
        llvm
        # Debuggers.
        gdb
        valgrind
        # Environment tools.
        direnv
        # Language tools.
        shellcheck
        # Misc tools.
        jq
        unetbootin
        xxd
        # Vcs.
        git
        git-lfs
        tig
        subversion
        # Network tools
        filezilla
      ];
      environment.pathsToLink = [
        "/share/nix-direnv"
      ];
      #services.lorri.enable = true; #https://github.com/NixOS/nixpkgs/issues/332957
    }) (
      mkIf (!cfg.developer.enable) {
        marti.roles.developer.android.enable = mkForce false;
        marti.roles.developer.database.enable = mkForce false;
        marti.roles.developer.docker.enable = mkForce false;
        marti.roles.developer.infosec.enable = mkForce false;
        marti.roles.developer.nix.enable = mkForce false;
      }
    )];

  imports = [
    (import ./developer/android.nix { inherit cfg lib pkgs; })
    (import ./developer/database.nix { inherit cfg lib pkgs; })
    (import ./developer/docker.nix { inherit cfg lib pkgs; })
    (import ./developer/infosec.nix { inherit cfg lib pkgs; })
    (import ./developer/nix.nix { inherit cfg lib pkgs; })
  ];

}

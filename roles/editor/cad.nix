{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.editor.cad = {
    enable = mkEnableOption "Wether to enable cad programs.";
  };

  config = mkIf cfg.editor.cad.enable {
    environment.systemPackages = with pkgs; [
      blender  # 3D creation suite
      # brlcad   # powerful cross-platform open source combinatorial solid modeling system # broken 2025-01-25
      freecad  # 3D parametric modeler
      #kicad    # Free Software EDA Suite
      librecad # 2D CAD package based upon Qt
      openscad # The Programmers Solid 3D CAD Modeller
      qcad     # 2D CAD package based on Qt
    ];
  };

}

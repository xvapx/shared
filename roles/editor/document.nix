{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.editor.document = {
    enable = mkEnableOption "Wether to enable document editors.";
    scan = {
      users = mkOption {
        type = types.listOf types.str;
        default = [ ];
        description = "List of users allowed to scan documents.";
      };
      backends = mkOption {
        type = types.listOf types.package;
        default = with pkgs; [ sane-airscan ];
        description = ''
          scanner backends to enable, see 
          http://www.sane-project.org/sane-supported-devices.html for details,
          use "scanimage -L" to determine if your scanner is supported by sane.
        '';
      };
    };
    dictionaries = mkOption {
      type = types.listOf types.package;
      default = with pkgs.aspellDicts; [
        en en-computers en-science
        es
        ca
      ];
      description = "dictionaries to include in the system.";
    };
  };

  config = mkIf cfg.editor.document.enable {
    environment.systemPackages = with pkgs; [
      aspell
      #calibre broken 2024-10-22
      libreoffice-fresh
      onlyoffice-bin
      pdftk
      poppler_utils
      qpdf
      scribus
      sigil
      skanlite
      qnotero
      zotero
      texstudio
      texmaker
      texliveFull
    ] ++ cfg.editor.document.dictionaries;
    services = {
      # dict.org daemon
      dictd = {
        enable = true;
        DBs = with pkgs.dictdDBs; [ wordnet wiktionary ];
      };
      # Enable network scanners.
      avahi = {
        enable = true;
        nssmdns4 = true;
      };
    };
    # Enable scanner service.
    # https://nixos.wiki/wiki/Scanners
    hardware.sane = {
      enable = true;
      extraBackends = cfg.editor.document.scan.backends;
      # TODO: parametrize brscan4, etc.
    };
    # Add scan users to "scanner" and "lp" groups.
    users.extraUsers = builtins.listToAttrs (
      map
        ( user: {
            name = user;
            value = {
              extraGroups = [ "scanner" "lp" ];
            };
          } )
        cfg.editor.document.scan.users );
    # If GIMP is installed, allow it to scan.
    nixpkgs.config.packageOverrides = pkgs: {
      xsaneGimp = pkgs.xsane.override { gimpSupport = true; };
    };
    # Add the GIMP xsane plugin to scan users' homes.
    #TODO: automatically 
    # ln -s /run/current-system/sw/bin/xsane ~/.config/GIMP/2.10/plug-ins/xsane
  };

}

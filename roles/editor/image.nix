{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.editor.image = {
    enable = mkEnableOption "Wether to enable image edition programs.";
  };

  config = mkIf cfg.editor.image.enable {
    environment.systemPackages = with pkgs; [
      darktable
      fontforge
      gimp
      inkscape
      krita
    ];
  };

}

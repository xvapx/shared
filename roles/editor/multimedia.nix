{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.editor.multimedia = {
    enable = mkEnableOption "Wether to enable multimedia edition programs.";
  };

  config = mkIf cfg.editor.multimedia.enable {
    environment.systemPackages = with pkgs; [
      abcde
      aegisub
      audacity
      beets
      cuetools
      ffmpeg
      flac
      flacon
      handbrake
      imagemagickBig
      kdenlive
      lmms
      mediainfo
      mkvtoolnix
      obs-studio
      shotcut
      shntool
      subtitleedit
    ];
  };

}

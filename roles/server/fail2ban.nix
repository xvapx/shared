{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.server.fail2ban = {
    enable = mkEnableOption "Wether to enable the fail2ban server role.";
    services = {
      nginx = mkEnableOption "Wether to enable nginx monitoring.";
    };
    increment = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = "Wether to increment bantime for repeat offenders.";
      };
      random = mkOption {
        type = types.int;
        default = (60 * 30);
        description = "Max random time to add to bantimes, in seconds.";
      };
      max = mkOption {
        type = types.int;
        default = (60 * 60 * 24 * 30);
        description = "Max duration for bantimes, in seconds.";
      };
    };
  };

  config = mkIf cfg.server.fail2ban.enable {
    services.fail2ban = {
      enable = true;
      bantime-increment = with cfg.server.fail2ban.increment; {
        inherit enable;
        rndtime = "${toString random}";
        maxtime = "${toString max}";
        overalljails = true;
      };
      jails = {
        recidive = ''
          enabled = true
          backend = systemd
          filter = recidive
          action = iptables-allports
          maxretry = 2
          findtime = 3d
          bantime = ${toString cfg.server.fail2ban.increment.max}
        '';
        nginx = ''
          enabled = ${lib.boolToString cfg.server.fail2ban.enable}
          backend = systemd
          filter = marti-nginx
          action = iptables-multiport[name=nginx, port="http,https", protocol=tcp]
          maxretry = 3
        '';
      };
    };
    # Add custom fail2ban filters
    # TODO: Add more nginx filters for req-limit, etc
    environment.etc = {
      "fail2ban/filter.d/marti-nginx.conf".text = ''
[Definition]

# format:
# hostname nginx[000000]:
__prefix_line =  \S+\snginx\[\d+\]:\s*

prefregex = ^%(__prefix_line)s

failregex = ^%(__prefix_line)s \d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}:\d{2}\s\[error]\s\d+#\d+:\s\*\d+\suser\s"(?:[^"]+|.*?)":?\s(?:password mismatch|was not found in "[^\"]*"),\sclient:\s<HOST>, server: \S*, request: "\S+ \S+ HTTP\/\d+\.\d+", host: "\S+"(?:, referrer: "\S+")?\s*

ignoreregex = 

datepattern = {^LN-BEG}

journalmatch = _SYSTEMD_UNIT=nginx.service

# DEV NOTES:
# Based on samples in https://github.com/fail2ban/fail2ban/pull/43/files
# Extensive search of all nginx auth failures not done yet.
# 
# Author: Daniel Black

# Modified by: Marti Serra
# Original from: https://github.com/fail2ban/fail2ban/blob/f81f85569d88da74d76f286b431dc54d0ae00748/config/filter.d/nginx-http-auth.conf
      '';
    };
  };

}

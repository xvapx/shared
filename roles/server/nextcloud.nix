# TODO: enable nginx

{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.server.nextcloud = {
    enable = mkEnableOption "Wether to enable the nextcloud server role.";
    package = mkOption {
      type = types.package;
      default = pkgs.nextcloud23;
      description = "nextcloud package to use.";
    };
    host = mkOption {
      type = types.str;
      default = "localhost";
      description = "nextcloud host.";
    };
    directories = {
      data = mkOption {
        type = types.path;
        default = "/var/lib/nextcloud";
        description = "nextcloud data directory.";
      };
      backup = mkOption {
        type = types.path;
        default = "/var/backups/nextcloud";
        description = "nextcloud backup directory.";
      };
    };
    log_level = mkOption {
      type = types.int;
      default = 2;
      description = "nextcloud log level.";
    };
    use_https = mkOption {
      type = types.bool;
      #TODO: Enable https by default
      default = false;
      description = "Wether nextcloud must use a secure connection.";
    };
    db = {
      name = mkOption {
        type = types.str;
        default = "nextcloud";
        description = "nextcloud database name.";
      };
      user = mkOption {
        type = types.str;
        default = "nextcloud";
        description = "nextcloud database user.";
      };
      host = mkOption {
        type = types.str;
        default = "/run/postgresql";
        description = "nextcloud database host.";
      };
      admin = mkOption {
        type = types.str;
        default = "root";
        description = "nextcloud database admin name.";
      };
      admin_password_file = mkOption {
        type = types.path;
        description = "nextcloud database admin password file.";
      };
    };
  };


  config = mkIf cfg.server.nextcloud.enable {
    services = with cfg.server.nextcloud; {
      # Set up nextcloud service.
      nextcloud = {
        enable = true;
        hostName = host;
        home = directories.data;
        logLevel = log_level;
        https = use_https;
        inherit package;
        config = {
          dbtype = "pgsql";
          dbname = db.name;
          dbuser = db.user;
          dbhost = db.host;
          adminuser = db.admin;
          adminpassFile = db.admin_password_file;
        };
      };
      # Set up postgresql server.
      postgresql = with cfg.server.nextcloud; {
        enable = true;
        ensureDatabases = [ db.name ];
        ensureUsers = [
          {
            name = db.user;
            ensurePermissions."DATABASE ${db.name}" = "ALL PRIVILEGES";
          }
        ];
      };
      # backup nextcloud, check this before restoring a backup:
      # https://docs.nextcloud.com/server/15/admin_manual/maintenance/restore.html
      # TODO: use hard links to make incremental copies with rsync
      cron = with cfg.server.nextcloud; {
        enable = true;
        systemCronJobs = [
          # https://docs.nextcloud.com/server/15/admin_manual/maintenance/backup.html
          # postgres db
          ''0 2 * * * root ${package}/bin/pg_dump ${db.name} | ${pkgs.gzip}/bin/gzip -c > ${directories.backup}/nextcloud_db_`date +"%Y-%m-%d"`.gz''
          # data dir
          ''30 2 * * * root ${pkgs.rsync}/bin/rsync -az ${directories.data} ${directories.backup}/nextcloud_dir_`date +"%Y-%m-%d"`/''
        ];
      };
    };
    # Ensure that postgres is running before running the setup.
    systemd.services."nextcloud-setup" = {
      requires = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
    };
    # Ensure the backup dir exists and has correct permissions on system activation.
    system.activationScripts.nextcloudBackupDir = with cfg.server.nextcloud.directories; {
      text = ''
        mkdir -m 0700 -p ${backup}
        chown root ${backup}
      '';
      deps = [ ];
    };
    # Open needed firewall ports.
    networking.firewall.allowedTCPPorts = [ 80 443 ];
  };

}

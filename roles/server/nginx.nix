{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.server.nginx = {
    enable = mkEnableOption "Wether to enable the nginx server role.";
    hosts = mkOption {
      type = types.attrsOf types.anything;
      default = {};
      description = "nginx VirtualHosts.";
    };
  };

  config = mkIf cfg.server.nginx.enable {
    services = {
      nginx = {
        enable = true;
        recommendedGzipSettings = true;
        recommendedOptimisation = true;
        recommendedProxySettings = true;
        recommendedTlsSettings = true;
        virtualHosts = cfg.server.nginx.hosts;
      };
    };
    # TODO: Open firewall ports.
    # extract hosts.<name>.listen ports
    networking.firewall.allowedTCPPorts = [ ];
  };

}

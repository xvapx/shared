{ cfg, lib, pkgs }:

# https://github.com/NixOS/hydra
# To run hydra, after enabling this role and rebuilding the system, run:
# su - hydra
# hydra-create-user <USER> --full-name '<NAME>' --email-address '<EMAIL>' --password-prompt --role admin

# Generate the signing key with:
# nix-store --generate-binary-cache-key cache.<ORGANIZATION>.com-01 /var/cache-key-01 /var/cache-key-01.pub
# chown hydra:keys /var/cache-key-01{,.pub}
# mkdir /var/lib/hydra-cache
# chown hydra-queue-runner:hydra /var/lib/hydra-cache


with lib;

{

  options.marti.roles.server.hydra = {
    enable = mkEnableOption "Wether to enable the hydra server role.";
    package = mkOption {
      type = types.package;
      default = pkgs.hydra-unstable;
      description = "hydra package to use.";
    };
    # hydra database settings
    db = {
      manage = mkEnableOption ''
        Wether to let this module automatically manage the database.
        Enabling this option is not compatible with using custom values for
        database host, port, name & user, and will ignore these custom values.
      '';
      host = mkOption {
        type = types.str;
        default = "localhost";
        description = "postgresql host.";
      };
      port = mkOption {
        type = types.int;
        default = 5432;
        description = "postgresql port.";
      };
      name = mkOption {
        type = types.str;
        default = "hydra";
        description = "postgresql database name.";
      };
      user = mkOption {
        type = types.str;
        default = "hydra";
        description = "postgresql database user.";
      };
    };
    # hydra web settings
    web = {
      host = mkOption {
        type = types.str;
        default = "127.0.0.1";
        description = "hydra webserver host.";
      };
      port = mkOption {
        type = types.int;
        default = 3000;
        description = "hydra webserver port.";
      };
      url = mkOption {
        type = types.str;
        default = "example.com/hydra";
        description = "hydra webserver url.";
      };
    };
    serve = {
      port = mkOption {
        type = types.int;
        default = 5000;
        description = "nix-serve port.";
      };
      key = mkOption {
        type = types.path;
        default = "/var/cache-key-01";
        description = "path to the signing key";
      };
    };
    space_queue = mkOption {
      type = types.int;
      default = 10;
      description = "minimum disk space to run the queue runner, in Gib.";
    };
    space_evaluator = mkOption {
      type = types.int;
      default = 10;
      description = "minimum disk space to run the evaluator, in Gib.";
    };
    gc_roots_dir = mkOption {
      type = types.path;
      default = "/nix/var/nix/gcroots/hydra";
      description = "garbage collection roots directory.";
    };
    extra_config = mkOption {
      type = types.lines;
      default = ''
        binary_cache_public_uri http://127.0.0.1
        max_output_size = ${toString (50 * 1024 * 1024 * 1024)}
      '';
      description = "extra configuration for hydra.";
    };
    extra_env = mkOption {
      type = types.attrsOf types.str;
      default = { };
      description = "extra environment variables for hydra.";
    };
    # hydra mail sender settings
    mail = {
      address = mkOption {
        type = types.str;
        default = "";
        description = "email address used for email notifications.";
      };
      host = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "smtp host used for email notifications.";
      };
    };
    use_binary_cache = mkEnableOption "Wether to get packages from the system configured binary cache(s) if avaliable";
    nix = {
      max_jobs = mkOption {
        type = types.int;
        default = 1;
        description = "maximum build jobs to run concurrently.";
      };
      tarball_ttl = mkOption {
        type = types.int;
        default = 0;
        description = "time for a tarball to be considered fresh, in seconds. 0 forces to download always.";
      };
      buildMachines = mkOption {
        type = types.listOf (types.attrsOf types.anything);
        default = [{
          hostName = "localhost";
          systems = [ "builtin" "x86_64-linux" "i686-linux" ];
          supportedFeatures = [ "local" "kvm" "nixos-test" "big-parallel" "benchmark" ];
          maxJobs = cfg.server.hydra.nix.max_jobs;
        }];
      };
    };
    nginx = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Wether to enable nginx reverse proxy for hydra and nix-serve.";
      };
      authFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        description = "path to the nginx authfile or null for no basic auth.";
      };
    };
  };

  config = mkIf cfg.server.hydra.enable {
    # Nix options
    nix = {
      extraOptions = ''
        tarball-ttl = ${toString cfg.server.hydra.nix.tarball_ttl}
        auto-optimise-store = true
        builders-use-substitutes = ${if cfg.server.hydra.use_binary_cache then "true" else "false"}
        secret-key-files = ${cfg.server.hydra.serve.key}
        experimental-features = nix-command flakes
      '';
      settings.trusted-users = [ "hydra" "hydra-evaluator" "hydra-queue-runner" ];
      distributedBuilds = true;
      buildMachines = cfg.server.hydra.nix.buildMachines;
    };
    services = {
      # Set up hydra service.
      hydra = with cfg.server.hydra; {
        enable = true;
        inherit package;
        dbi = (mkMerge [
          (mkIf db.manage "dbi:Pg:dbname=hydra;user=hydra;")
          (mkIf (!db.manage) "dbi:Pg:dbname=${db.name};host=${db.host};port=${toString db.port};user=${db.user}")
        ]);
        hydraURL = web.url;
        listenHost = web.host;
        port = web.port;
        minimumDiskFree = space_queue;
        minimumDiskFreeEvaluator = space_evaluator;
        notificationSender = mail.address;
        smtpHost = mail.host;
        extraConfig = extra_config + ''
          store_uri = file:///var/lib/hydra-cache?secret-key=${serve.key}
        '';
        extraEnv = extra_env;
        gcRootsDir = gc_roots_dir;
        useSubstitutes = use_binary_cache;
        buildMachinesFiles = [];
      };
      # Set up nix-serve service.
      nix-serve = with cfg.server.hydra.serve; {
        enable = true;
        secretKeyFile = key;
        inherit port;
      };
      # Restart hydra-queue-runner daily at 03:00 as root.
      cron = {
        enable = true;
        systemCronJobs = [
          "0 3 * * * root systemctl restart hydra-queue-runner.service >> /var/log/cron-hydra.log"
        ];
      };
    };
    marti.roles.server = {
      # nginx reverse proxy
      nginx = {
        enable = true;
        hosts = {
          "coop.trevol.com" = with cfg.server.hydra; {
            listen = [ {
              addr = "0.0.0.0";
              port = 80;
            } {
              addr = "0.0.0.0";
              port = 443;
            }];
            locations."/hydra/" = {
              proxyPass = "http://${web.host}:${toString web.port}";
              extraConfig = ''
                proxy_redirect http://${web.host}:${toString web.port} ${web.url};
                proxy_set_header  Host              $host;
                proxy_set_header  X-Real-IP         $remote_addr;
                proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
                proxy_set_header  X-Forwarded-Proto $scheme;
                proxy_set_header  X-Request-Base    /hydra;
                rewrite ^/hydra(.*) /$1 break;
              '' + (if nginx.authFile != null then ''
                  auth_basic "hydra";
                  auth_basic_user_file ${nginx.authFile};
                '' else "");
            };
            enableACME = true;
          };
        };
      };
      # fail2ban for nginx
      fail2ban = {
        enable = true;
        services.nginx = true;
      };
    };
    # Open firewall ports.
    networking.firewall.allowedTCPPorts = with cfg.server.hydra; [ web.port serve.port ];
    # Configure ssl certificates
    security.acme = {
      # TODO: Move everything non-hydra-specific to an appropiate file
      # Accept Let's Encrypt's terms of service
      acceptTerms = true;
      defaults = {
        email = "${cfg.server.hydra.mail.address}";
      };
    };
    # Allow hydra users to acces keys.
    users.users = {
      hydra.extraGroups = [ "keys" ];
      hydra-queue-runner.extraGroups = [ "keys" ];
    };
  };

}

{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.server.amule = {
    enable = mkEnableOption "Wether to enable the amule server role.";
    openFirewall = mkEnableOption "Wether to open the firewall ports used by amuled." // { default = true; };
    dataDir = mkOption {
      type = types.path;
      default = ''/home/${user}/'';
      description = ''
        The directory holding configuration, incoming and temporary files.
      '';
    };
    user = mkOption {
      type = types.str;
      default = "amule";
      description = ''
        The user the Amule daemon should run as.
      '';
    };
    package = mkOption {
      type = types.package;
      default = pkgs.amule-daemon;
      description = ''
        Amule package to use.
      '';
    };
  };

  config = mkIf cfg.server.amule.enable {

    users = {
      users = mkIf (cfg.user == "amule") {
        amule = {
          description = "Amule daemon";
          group = "amule";
          uid = cfg.ids.uids.amule;
        };
      };
      groups = mkIf (cfg.user == "amule") {
        amule = {
          gid = config.ids.gids.amule;
        };
      };
    };

    systemd.services.amuled = {
      description = "Amule daemon";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];

      preStart = ''
        mkdir -p ${cfg.dataDir}
        chown ${cfg.user} ${cfg.dataDir}
      '';

      script = ''
        ${pkgs.su}/bin/su -s ${pkgs.runtimeShell} ${cfg.user} \
          -c 'HOME="${cfg.dataDir}" ${cfg.package}/bin/amuled'
      '';
    };

    # Open firewall ports.
    networking.firewall = mkIf (cfg.openFirewall == true) {
      allowedTCPPorts = [ 4712 4662 ];
      allowedUDPPorts = [ 4665 ];
    };
  };

}

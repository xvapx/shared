{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.common = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = ''
        Wether to enable the common role, true by default.
      '';
    };
    version_label = mkOption {
      type = types.str;
      default = "";
      description = ''
        text to add to system.nixos.label, "" by default.
      '';
    };
    nix_package = mkOption {
      type = types.nullOr types.package;
      default = pkgs.nixVersions.latest;
      description = "nix package use system-wide, use null to define it elsewhere.";
    };
  };

  config = mkIf cfg.common.enable ( mkMerge [ {
    # System-wide pacakges.
    environment.systemPackages = with pkgs; [
      # Improved versions of basic tools
      bat
      eza
      fd
      fzf
      ncdu
      pigz
      ripgrep
      ripgrep-all
      tldr
      # Basic tools
      aria2
      bc
      bcachefs-tools
      cifs-utils
      cdrkit
      cmus
      curl
      diskscan
      dmidecode
      elinks
      exfat
      fasd
      file
      git
      hwinfo
      pinentry-curses
      gnupg
      hddtemp
      hdparm
      htop
      httpie
      inetutils
      iperf
      iptables
      lm_sensors
      keepassxc
      lnav
      lshw
      lsof
      man-pages
      man-pages-posix
      mediainfo
      mc
      nano
      neofetch
      nmap
      nnn
      ntfs3g
      pass
      p7zip
      parted
      patchelf
      pcalc
      pciutils
      progress
      psmisc
      rename
      rsync
      rclone
      # s-tar # Broken 2025-01-25
      screen
      sdparm
      smartmontools
      sourceHighlight
      stow
      strace
      syslinux
      taskwarrior3
      inetutils
      tmux
      tree
      unar
      units
      unrar
      unshield
      usbutils
      ventoy-full
      vim
      wget
      zellij
      zip
      # Nix tools
      nix-index
      nix-du
      nix-prefetch-scripts
      nox
    ];
    # Enable nix flakes.
    nix = {
      extraOptions = "experimental-features = nix-command flakes";
    };
    # Enable basic tools.
    programs = {
      bash.completion.enable = true;
      tmux = import ./common/tmux_config.nix { inherit pkgs; };
      criu.enable = true;
      less.enable = true;
    };
    # Enable basic services.
    services = {
      # Service to auto-update the file database used by locate.
      locate = {
        package = pkgs.mlocate;
        localuser = null;
        enable = true;
        # update at 4:00 daily
        interval = "04:00";
      };
      openssh = {
        # Self-generated ssh moduli file
        moduliFile = "${pkgs.ssh-moduli}/moduli";
      };
      fwupd = {
        enable = true;
      };
    };
    # Define default XDG directories.
    environment.variables = {
      XDG_CACHE_HOME = mkDefault "$HOME/.cache";
      XDG_CONFIG_HOME = mkDefault "$HOME/.config/";
      XDG_DATA_HOME = mkDefault "$HOME/.local/share";
      XDG_STATE_HOME = mkDefault "$HOME/.local/state";
      FONTCONFIG_FILE = mkDefault "${pkgs.fontconfig.out}/etc/fonts/fonts.conf";
      FONTCONFIG_PATH = mkDefault "${pkgs.fontconfig.out}/etc/fonts/";
    };
    # Install common fonts.
    fonts = {
      fontDir.enable = true;
      enableGhostscriptFonts = false;
      fontconfig = {
        defaultFonts = {
          monospace = [ "Noto Mono" ];
          sansSerif = [ "Open Sans" ];
          serif = [ "Linux Libertine" ];
        };
      };
      packages = with pkgs; [
        caladea
        corefonts
        dejavu_fonts
        freefont_ttf
        google-fonts
        hack-font
        inconsolata
        libertine
        open-sans
        noto-fonts
        noto-fonts-cjk-sans
        noto-fonts-emoji
        terminus_font
        unifont
      ];
    };
    # Add bindfs to packages supplying file system mounters and checkers
    system.fsPackages = [ pkgs.bindfs ];
    # mount fonts and icons to expected FHS directories
    fileSystems = let
      mkRoSymBind = path: {
        device = path;
        fsType = "fuse.bindfs";
        options = [ "ro" "resolve-symlinks" "x-gvfs-hide" ];
      };
      aggregatedIcons = pkgs.buildEnv {
        name = "system-icons";
        paths = with pkgs; [
          kdePackages.breeze
          gnome-themes-extra
        ];
        pathsToLink = [ "/share/icons" ];
      };
      aggregatedFonts = pkgs.buildEnv {
        name = "system-fonts";
        paths = cfg.fonts.packages;
        pathsToLink = [ "/share/fonts" ];
      };
    in {
      "/usr/share/icons" = mkRoSymBind "${aggregatedIcons}/share/icons";
      "/usr/local/share/fonts" = mkRoSymBind "${aggregatedFonts}/share/fonts";
    };
    # enable shell on boot fail
    boot.kernelParams = [ "boot.shell_on_fail" ];
    # Add version label to the grub entry.
    system.nixos.label = cfg.common.version_label;
  } ( mkIf (cfg.common.nix_package != null) {
    nix.package = cfg.common.nix_package;
  }) ] );
}


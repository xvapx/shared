{ cfg, lib, pkgs }:

with lib;

let
  # tools for gaming systems
  myTools = with pkgs; [
    gamemode
    mangohud
    timidity
    inotify-tools
    scummvm
    dosbox-staging
    pixman
  ];
  # Custom steam
  mySteam = pkgs.steam.override {
    extraLibraries = pkgs: with cfg.hardware.graphics;
      if pkgs.hostPlatform.is64bit
      then [ package ] ++ extraPackages
      else [ package32 ] ++ extraPackages32;
    extraPkgs = pkgs: with pkgs; myTools;
  };
in {

  options.marti.roles.gaming = {
    enable = mkEnableOption "Wether to enable gaming software.";
    users = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "List of users allowed to use gamepads & joysticks.";
    };
    steam = {
      remotePlay = mkEnableOption "Wether to enable Steam remote play compatibility.";
      controller = mkEnableOption "Wether to enable steam controller compatibility.";
    };
    sony = {
      dualshock4 = mkEnableOption "Wether to enable dualshock 4 compatibility.";
    };
    stadia = {
      controller = mkEnableOption "Wether to enable stadia controller compatibility";
    };
  };

  config = mkIf cfg.gaming.enable {
    programs = {
      cdemu.enable = true;
      gamemode = {
        enable = true;
        enableRenice = true;
        settings = {
          general.renice = 10;
        };
      };
    };
    environment = {
      systemPackages = with pkgs; [
        # game managing
        mySteam
        mySteam.run
        # package extractors
        cabextract
        innoextract
        # vulkan tools
        vulkan-loader
        vulkan-tools
        # compatibility tools
        winetricks
        protontricks
        protonup-qt
        wineWowPackages.staging
        # Hardware
        evtest
        sdl-jstest
        qjoypad
      ] ++ (if cfg.profiles.network.bluetooth.enable then [
        bluez
      ] else [ ]) ++ myTools;
      variables = {
        SDL_JOYSTICK_DEVICE = "/dev/input/js0";
      };
    };
    # Open firewall ports for steam remote play if enabled
    networking.firewall = {
      allowedTCPPorts = (mkMerge [
        (mkIf cfg.gaming.steam.remotePlay [ 27036 27037 ])
      ]);
      allowedUDPPorts = (mkMerge [
        (mkIf cfg.gaming.steam.remotePlay [ 27031 27036 ])
      ]);
    };
    services.udev.extraRules = (if cfg.gaming.steam.controller then ''

      # Steam Controller
      SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", MODE="0666"
      KERNEL=="uinput", MODE="0660", GROUP="users", OPTIONS+="static_node=uinput"

      # Valve HID devices over USB hidraw
      KERNEL=="hidraw*", ATTRS{idVendor}=="28de", MODE="0666"

      # Valve HID devices over bluetooth hidraw
      KERNEL=="hidraw*", KERNELS=="*28DE:*", MODE="0666"

    '' else '''') + (if cfg.gaming.sony.dualshock4 then ''

      # DualShock 4 over USB hidraw
      KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="05c4", MODE="0666"

      # DualShock 4 wireless adapter over USB hidraw
      KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0ba0", MODE="0666"

      # DualShock 4 Slim over USB hidraw
      KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="09cc", MODE="0666"

      # DualShock 4 over bluetooth hidraw
      KERNEL=="hidraw*", KERNELS=="*054C:05C4*", MODE="0666"

      # DualShock 4 Slim over bluetooth hidraw
      KERNEL=="hidraw*", KERNELS=="*054C:09CC*", MODE="0666"

    '' else '''') + (if cfg.gaming.stadia.controller then ''

      KERNEL=="hidraw*", ATTRS{idVendor}=="1fc9", MODE="0666"
      ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="1fc9", MODE="0666"
      ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="0d28", MODE="0666"

      # Flashloader
      KERNEL=="hidraw*", ATTRS{idVendor}=="15a2", MODE="0666"

      # Controller
      KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="18d1", MODE="0666"
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="18d1", ATTRS{idProduct}=="9400", MODE="0660", TAG+="uaccess"

    '' else '''');
    # optionally bluetooth to support gaming hardware
    hardware = {
      bluetooth = {
        enable = cfg.profiles.network.bluetooth.enable;
        package = pkgs.bluez;
      };
    };
    # lutris wine needs this
    security.pam.loginLimits = [{
      domain = "*";
      type = "-";
      item = "nofile";
      value = "524288";
    }];
    # add gaming users to "input" and "cdrom" groups 
    users.extraUsers = builtins.listToAttrs (
      map
        ( user: {
            name = user;
            value = {
              extraGroups = [ "input" "cdrom" ];
            };
          } )
        cfg.gaming.users );
    # Enable transparent hugepages
    boot.kernelParams = [ "transparent_hugepages=always" ];
  };

}

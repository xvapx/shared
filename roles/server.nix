{ cfg, lib, pkgs }:

with lib;

{
  options.marti.roles.server.enable = mkOption {
    type = types.bool;
    default = true;
    description = "Wether to enable marti's shared server roles.";
  };

  config = mkIf (!cfg.server.enable) {
    marti.roles.server.amule.enable = mkForce false;
    marti.roles.server.fail2ban.enable = mkForce false;
    marti.roles.server.hydra.enable = mkForce false;
    marti.roles.server.nextcloud.enable = mkForce false;
    marti.roles.server.nginx.enable = mkForce false;
  };

  imports = [
    (import ./server/amule.nix { inherit cfg lib pkgs; })
    (import ./server/hydra.nix { inherit cfg lib pkgs; })
    (import ./server/fail2ban.nix { inherit cfg lib pkgs; })
    (import ./server/nextcloud.nix { inherit cfg lib pkgs; })
    (import ./server/nginx.nix { inherit cfg lib pkgs; })
  ];

}

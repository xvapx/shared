{ cfg, lib, pkgs, secrets }:

with lib;

let

  general_config = {
      # Protocol: "tcp" or "udp"
      proto ? "tcp"
      # Mode: "tap" or "tun"
      , mode ? "tun"
      # Certificate authority file path
      , ca
      # Authentication file path
      , auth
      # Compression algorithm, "lz4", "lzo" or "" (disabled)
      , compress ? ""
      # Verbosity
      , verb ? "3"
      # Enable IPv6?
      , ipv6 ? false
      # Remote servers
      , remotes
      # Ciphers
      , data-ciphers ? ""
    }: { 
      config = ''
        client
        proto ${proto}${if ipv6 then "" else "4"}
        ${if ipv6 then ''
          tun-ipv6
        '' else ''
          pull-filter ignore "route-ipv6"
          pull-filter ignore "ifconfig-ipv6"
          pull-filter ignore "tun-ipv6"
        ''}
        ${if proto == "udp" then "fast-io" else ""}
        dev ${mode}
        resolv-retry infinite
        nobind
        persist-key
        persist-tun
        remote-cert-tls server
        ca ${ca}
        auth-user-pass ${auth}
        reneg-sec 3600
        ${if compress == "" then "allow-compression no" else ''
          compress ${compress}
          allow-compression asym
        ''}
        verb ${verb}
        ping 10
        ping-restart 60
        sndbuf 524288
        rcvbuf 524288
        script-security 2
        ${if data-ciphers != "" then "data-ciphers ${data-ciphers}" else ""}
      '' + remotes;
      updateResolvConf = true;
      autoStart = mkDefault false;
  };

  mullvad_config = remotes: general_config {
    inherit remotes;
    proto = "udp";
    mode = "tun";
    data-ciphers = "AES-256-GCM:AES-256-CBC";
    ipv6 = cfg.vpn.mullvad.ipv6;
    auth = if cfg.vpn.mullvad.auth != null then cfg.vpn.mullvad.auth else secrets.marti_mullvad_auth.path;
    ca = if cfg.vpn.mullvad.ca != null then cfg.vpn.mullvad.ca else secrets.marti_mullvad_ca.path;
  };

  getflix_config = proto: remotes: general_config {
    inherit remotes proto;
    mode = "tun";
    data-ciphers = "AES-256-GCM:AES-256-CBC";
    ipv6 = cfg.vpn.getflix.ipv6;
    auth = if cfg.vpn.getflix.auth != null then cfg.vpn.getflix.auth else secrets.marti_getflix_auth.path;
    ca = if cfg.vpn.getflix.ca != null then cfg.vpn.getflix.ca else secrets.marti_getflix_ca.path;
  };

  getflix_config_tcp = remotes: getflix_config "tcp" remotes;
  getflix_config_udp = remotes: getflix_config "udp" remotes;

in

{

  options.marti.roles.vpn = {
    enable = mkEnableOption "Wether to enable marti's shared vpn roles.";
    mullvad = {
      enable = mkEnableOption "Enable mullvad VPN?";
      ipv6 = mkEnableOption "Enable IPv6 for mullvad?";
      autostart = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Country code to connect mullvad to during autostart, null disables autostart.";
      };
      auth = mkOption {
        type = types.nullOr types.path;
        default = null;
        description = "Path to the auth file.";
      };
      ca = mkOption {
        type = types.nullOr types.path;
        default = null;
        description = "Path to the CA file.";
      };
    };
    getflix = {
      enable = mkEnableOption "Enable getflix VPN?";
      ipv6 = mkEnableOption "Enable IPv6 for getflix?";
      autostart = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Country code to connect getflix to during autostart, null disables autostart.";
      };
      auth = mkOption {
        type = types.nullOr types.path;
        default = null;
        description = "Path to the auth file.";
      };
      ca = mkOption {
        type = types.nullOr types.path;
        default = null;
        description = "Path to the CA file.";
      };
    };
  };

  config = mkMerge [
    (mkIf (!cfg.vpn.enable) {
      marti.roles.vpn.mullvad.enable = mkForce false;
      marti.roles.vpn.getflix.enable = mkForce false;
    })
    (mkIf cfg.vpn.mullvad.enable {
      # https://mullvad.net/en/account/#/openvpn-config/?platform=linux
      services.openvpn.servers.mullvad-es = mullvad_config ''
        remote-random
        remote es-mad-005.mullvad.net 1196
        remote es-mad-001.mullvad.net 1196
        remote es-mad-002.mullvad.net 1196
        remote es-mad-003.mullvad.net 1196
        remote es-mad-004.mullvad.net 1196
      '';
      services.openvpn.servers.mullvad-uk = mullvad_config ''
        remote-random
        remote gb-mnc-102.mullvad.net 1197
        remote gb-mnc-103.mullvad.net 1197
        remote gb-lon-009.mullvad.net 1197
        remote gb-lon-002.mullvad.net 1197
        remote gb-lon-003.mullvad.net 1197
        remote gb-lon-001.mullvad.net 1197
        remote gb-mnc-106.mullvad.net 1197
        remote gb-lon-008.mullvad.net 1197
        remote gb-mnc-101.mullvad.net 1197
        remote gb-lon-005.mullvad.net 1197
        remote gb-lon-101.mullvad.net 1197
        remote gb-lon-102.mullvad.net 1197
        remote gb-lon-006.mullvad.net 1197 '';
      services.openvpn.servers.mullvad-us = mullvad_config ''
        remote-random
        remote us-sjc-002.mullvad.net 1194
        remote us-lax-001.mullvad.net 1194
        remote us-lax-015.mullvad.net 1194
        remote us-dal-105.mullvad.net 1194
        remote us-slc-103.mullvad.net 1194
        remote us-mia-205.mullvad.net 1194
        remote us-lax-012.mullvad.net 1194
        remote us-lax-006.mullvad.net 1194
        remote us-lax-011.mullvad.net 1194
        remote us-nyc-213.mullvad.net 1194
        remote us-sea-103.mullvad.net 1194
        remote us-lax-005.mullvad.net 1194
        remote us-lax-007.mullvad.net 1194
        remote us-dal-103.mullvad.net 1194
        remote us-atl-102.mullvad.net 1194
        remote us-lax-101.mullvad.net 1194
        remote us-sea-102.mullvad.net 1194
        remote us-nyc-102.mullvad.net 1194
        remote us-chi-001.mullvad.net 1194
        remote us-lax-003.mullvad.net 1194
        remote us-phx-103.mullvad.net 1194
        remote us-nyc-203.mullvad.net 1194
        remote us-nyc-205.mullvad.net 1194
        remote us-chi-004.mullvad.net 1194
        remote us-atl-104.mullvad.net 1194
        remote us-rag-102.mullvad.net 1194
        remote us-dal-104.mullvad.net 1194
        remote us-nyc-212.mullvad.net 1194
        remote us-atl-002.mullvad.net 1194
        remote us-nyc-201.mullvad.net 1194
        remote us-dal-203.mullvad.net 1194
        remote us-nyc-202.mullvad.net 1194
        remote us-chi-002.mullvad.net 1194
        remote us-lax-004.mullvad.net 1194
        remote us-qas-002.mullvad.net 1194
        remote us-slc-104.mullvad.net 1194
        remote us-dal-201.mullvad.net 1194
        remote us-dal-204.mullvad.net 1194
        remote us-mia-203.mullvad.net 1194
        remote us-lax-002.mullvad.net 1194
        remote us-rag-103.mullvad.net 1194
        remote us-nyc-209.mullvad.net 1194
        remote us-dal-102.mullvad.net 1194
        remote us-nyc-214.mullvad.net 1194
        remote us-mia-204.mullvad.net 1194
        remote us-lax-102.mullvad.net 1194
        remote us-lax-008.mullvad.net 1194
        remote us-nyc-218.mullvad.net 1194
        remote us-lax-009.mullvad.net 1194
        remote us-nyc-207.mullvad.net 1194
        remote us-phx-102.mullvad.net 1194
        remote us-sjc-003.mullvad.net 1194
        remote us-nyc-216.mullvad.net 1194
        remote us-den-003.mullvad.net 1194
        remote us-den-001.mullvad.net 1194
        remote us-mia-206.mullvad.net 1194
        remote us-nyc-206.mullvad.net 1194
        remote us-atl-101.mullvad.net 1194
        remote us-qas-001.mullvad.net 1194
        remote us-chi-005.mullvad.net 1194
        remote us-mia-202.mullvad.net 1194
        remote us-slc-106.mullvad.net 1194
        remote us-den-002.mullvad.net 1194
        remote us-mia-201.mullvad.net 1194
        '';
    })
    (mkIf (cfg.vpn.mullvad.autostart != null) {
      services.openvpn.servers."mullvad-${cfg.vpn.mullvad.autostart}".autostart = mkforce true;
    })
    (mkIf cfg.vpn.getflix.enable {
      # https://network.glbls.net/openvpnconfig/Getflix-OpenVPN.zip
      services.openvpn.servers.getflix-es = getflix_config_udp ''
        remote es-md-smart.serverlocation.co 1194
      '';
      services.openvpn.servers.getflix-uk = getflix_config_udp ''
        remote-random
        remote uk-hl-smart.serverlocation.co 1194
        remote uk-lon1-smart.serverlocation.co 1194
        remote uk-ox2-smart.serverlocation.co 1194
      '';
      services.openvpn.servers.getflix-us = getflix_config_udp ''
        remote-random
        remote us-la3-smart.serverlocation.co 1194
        remote us-mn2-smart.serverlocation.co 1194
        remote us-ny1-smart.serverlocation.co 1194
        remote us-wdc2-smart.serverlocation.co 1194
      '';
    })
    (mkIf (cfg.vpn.getflix.autostart != null) {
      services.openvpn.servers."getflix-${cfg.vpn.getflix.autostart}".autostart = mkForce true;
    })
  ];

}

{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.virtualization.libvirtd-kvm = {
    enable = mkEnableOption "Wether to enable virtualization with libvirtd & kvm.";
    users = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "List of users allowed to manage libvirtd & kvm virtualization.";
    };
  };

  config = mkIf cfg.virtualization.libvirtd-kvm.enable {
    environment.systemPackages = with pkgs; [
      libguestfs-with-appliance
      virt-manager
      virt-viewer
      virt-top
    ];
    virtualisation.libvirtd = {
      enable = true;
      onShutdown = "shutdown";
      # Run qemu as root
      qemu = {
        verbatimConfig = ''
namespaces = []
user = "root"
group = "root"

cgroup_controllers = [ "cpu", "devices", "memory", "blkio", "cpuset", "cpuacct" ]

cgroup_device_acl = [
  "/dev/null", "/dev/full", "/dev/zero",
  "/dev/random", "/dev/urandom",
  "/dev/ptmx", "/dev/kvm",
  "/dev/rtc","/dev/hpet",
  "/dev/kvmfr0", "/dev/kvmfr1"
]
        '';
      };
    };
    boot = with cfg.virtualization.libvirtd-kvm; {
      kernel.sysctl = { "net.ipv4.ip_forward" = 1; };
      kernelModules = (mkMerge [
        (mkIf (cfg.profiles.cpu.vendor == "intel") [ "kvm-intel " ])
        (mkIf (cfg.profiles.cpu.vendor == "amd") [ "kvm-amd " ])
        [ ]
      ]);
    };
    # Enable qemu guest agent
    services.qemuGuest.enable = true;
    # Add virtualization users to "libvirtd" and "kvm" groups.
    users.extraUsers = builtins.listToAttrs (
      map
        (
          user: {
            name = user;
            value = {
              extraGroups = [ "libvirtd" "kvm" ];
            };
          } )
        cfg.virtualization.libvirtd-kvm.users );
    # export the path to the appliance
    environment.variables.LIBGUESTFS_PATH = "${pkgs.libguestfs-with-appliance}/lib/guestfs";
    # enable polkit
    security.polkit.enable = true;
  };

}

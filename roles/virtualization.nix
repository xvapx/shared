{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.virtualization.enable = mkOption {
    type = types.bool;
    default = true;
    description = "Wether to enable marti's shared virtualization roles.";
  };

  config = mkIf (!cfg.virtualization.enable) {
    marti.roles.virtualization.libvirtd-kvm.enable = mkForce false;
  };

  imports = [
    (import ./virtualization/libvirtd-kvm.nix { inherit cfg lib pkgs; })
  ];

}

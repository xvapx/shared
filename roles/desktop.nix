{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.desktop = {
    enable = mkEnableOption "Wether to enable desktop environment.";
    remote.client = mkEnableOption "Wether to enable remote desktop clients.";
  };


  config = mkMerge [
    (mkIf cfg.desktop.enable {
      # Common desktop packages.
      environment.systemPackages = with pkgs; [
        # Desktop functionality.
        pavucontrol
        xclip
        wl-clipboard-rs
        xdotool
        # Document viewers.
        evince
        comical
        mupdf
        # Encryption.
        veracrypt
        # Media players.
        amarok
        audacious
        clementine
        mpv
        rhythmbox
        strawberry
        vlc
        # Note taking applications.
        cherrytree
        # OpenGL.
        glxinfo
        # Partition editors.
        gparted
        # Password managers.
        keepassxc
        # Screenshot & screen capture utilities.
        maim
        shutter
        scrot
        # Terminal emulators.
        alacritty
        kitty
        # Web browsers.
        firefox-bin
        chromium
        google-chrome
        # World sphere viewers.
        googleearth-pro
        marble
        # Needed by some packages.
        dconf
        gnome-themes-extra
      ] ++ (if cfg.desktop.remote.client then [
        # Remote desktop clients.
        remmina
        tigervnc
        xrdp
      ] else [ ]);
      # Needed by some packages.
      environment.pathsToLink = [ "/etc/gconf" ];
      programs.dconf.enable = true;
      # Basic desktop services.
      services = {
        dbus = {
          enable = true;
          packages = [ pkgs.gnome2.GConf pkgs.dconf ];
        };
        xserver = {
          enable = true;
          autorun = true;
          exportConfiguration = true;
        };
        pipewire = {
          enable = true;
          alsa.enable = true;
          alsa.support32Bit = true;
          pulse.enable = true;
          jack.enable = true;
        };
      };
    }) (mkIf (!cfg.desktop.enable) {
      marti.roles.desktop.kde.enable = mkForce false;
      marti.roles.desktop.xfce.enable = mkForce false;
      marti.roles.desktop.gnome.enable = mkForce false;
      marti.roles.desktop.remote.server.enable = mkForce false;
    })];

  imports = [
    (import ./desktop/kde.nix { inherit cfg lib pkgs; })
    (import ./desktop/xfce.nix { inherit cfg lib pkgs; })
    (import ./desktop/gnome.nix { inherit cfg lib pkgs; })
    (import ./desktop/remote.nix { inherit cfg lib pkgs; })
  ];
}

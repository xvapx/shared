{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.flatpak = {
    enable = mkEnableOption "Wether to enable flatpak.";
  };

  config = mkIf cfg.flatpak.enable {
    services.flatpak.enable = true;
  };

}

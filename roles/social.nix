{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.social = {
    enable = mkEnableOption "Wether to enable messaging/chat programs.";
  };

  config = mkIf cfg.social.enable {
    environment.systemPackages = with pkgs; (mkMerge [
      [
        irssi
        neomutt
        signal-cli
      ]
      (mkIf cfg.desktop.enable [
        discord
        hexchat
        pidgin
        element-web
        signal-desktop
      ])
    ]);
  };

}

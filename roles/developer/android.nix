{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.developer.android = {
    enable = mkEnableOption "Wether to enable software for android developers.";
    users = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "List of android developer users.";
    };
  };

  config = mkIf cfg.developer.android.enable {
    environment.systemPackages = with pkgs; [
      android-tools
    ];
    # Enable adb.
    programs.adb.enable = true;
    # Enable android udev rules.
    services.udev.packages = [ pkgs.android-udev-rules ];
    # Add android developer users to "adbusers" group.
    users.extraUsers = builtins.listToAttrs (
      map
        ( user: {
            name = user;
            value = {
              extraGroups = [ "adbusers" ];
            };
          } )
        cfg.developer.android.users );
  };

}

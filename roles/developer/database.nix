{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.developer.database = {
    enable = mkEnableOption "Wether to enable software for database developers.";
    databases = mkOption {
      type = types.listOf (types.enum [ "postgresql" "mariadb" ]);
      default = [ "postgresql" "mariadb" ];
      description = "Database systems to develop for.";
    };
  };

  config = mkIf cfg.developer.database.enable {
    environment.systemPackages = with pkgs; [ ] ++
    (if builtins.elem "mariadb" cfg.developer.database.databases then [
      # mysql-workbench 2024-03-09 broken
      # mycli 2024-10-16 broken
      # TODO: pgcli
    ] else [ ]) ++ (if builtins.elem "postgresql" cfg.developer.database.databases then [
      pg_top
      #pgmodeler 2024-02-04 broken
    ] else [ ]);
  };

}

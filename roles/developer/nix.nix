{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.developer.nix = {
    enable = mkEnableOption "Wether to enable software for nix developers.";
  };

  config = mkIf cfg.developer.nix.enable {
    environment.systemPackages = with pkgs; [
      nix-diff
      nix-pin
      nix-tree
      nix-update
      nixfmt-classic
      nixfmt-rfc-style
      nixpkgs-fmt
    ];
  };

}

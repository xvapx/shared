{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.developer.infosec = {
    enable = mkEnableOption "Wether to enable software for infosec.";
  };

  config = mkIf cfg.developer.infosec.enable {
    environment.systemPackages = with pkgs; [
      burpsuite
      chkrootkit
      clamav
      hashcat
      httpie
      john
      lynis
      metasploit
      openssl
      openvpn
      testdisk
      wireshark
    ];
  };

}

{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.developer.docker = {
    enable = mkEnableOption "Wether to enable software for docker developers.";
  };

  config = mkIf cfg.developer.docker.enable {
    environment.systemPackages = with pkgs; [
      docker-compose
    ];
    virtualisation.docker = {
      enable = true;
      autoPrune.enable = true;
      extraOptions = "";
    };
  };

}

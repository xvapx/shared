# Note: firewall ports are not to be opened, xrdp should only
# be accessed from ssh using a tunnel, like with:
# ssh user@192.168.0.2 -p 22 -L 3333:127.0.0.1:3389 -N
# and then connecting the remote desktop client to 127.0.0.1:3333

# To connect to the RDP service:
# first start a tunnel from the client to the server with:
# ssh -t -L ${guest-port}:localhost:${host-port} -l ${remote-user} ${remote-host} -N
# and then connect your RDP client to localhost:${guest-port}

# To connect to the screen sharing service:
# first get the user to start the server launching the assistance program
# then open a tunnel from the client to the server with:
# ssh -t -L ${guest-port}:localhost:${host-port} -l ${remote-user} ${remote-host} -N
# and then connect your VNC client to localhost:${guest-port}

{ cfg, lib, pkgs }:

let

  # Override xrdp to allow only local connections.
  local-xrdp = pkgs.xrdp.overrideAttrs (oldAttrs: {
    preFixup = ''
      sed '/^\[Globals\]*/a address=127.0.0.1' $out/etc/xrdp/xrdp.ini
    '';
  });
  # Localized remote assitance.
  local-assistance = pkgs.assistance.override {
    lang = cfg.desktop.remote.assistance.language;
    port = cfg.desktop.remote.assistance.port;
  };
  # Remote desktop window-manager start command.
  remote_window_manager = if cfg.desktop.kde.enable then "startkde" else "xterm";

in

with lib;

{

  options.marti.roles.desktop.remote = {
    server = {
      enable = mkEnableOption "Wether to enable remote desktop service.";
      window_manager = mkOption {
        type = types.str;
        default = remote_window_manager;
        description = "window manager for the remote desktop service.";
      };
      port = mkOption {
        type = types.int;
        default = 3389;
        description = "port for the remote desktop service.";
      };
    };
    assistance = {
      enable = mkEnableOption "Wether to enable remote desktop assistance.";
      language = mkOption {
        type = types.str;
        default = "en";
        description = "language for the remote assistance program.";
      };
      port = mkOption {
        type = types.int;
        default = 2389;
        description = "port for the remote assistance service.";
      };
    };
  };


  config = mkIf cfg.desktop.remote.server.enable {
    environment.systemPackages = with pkgs; [
      local-assistance
    ];
    services.xrdp = with cfg.desktop.remote.server; {
      enable = true;
      package = local-xrdp;
      inherit port;
      defaultWindowManager = window_manager;
    };
  };

}

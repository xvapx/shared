{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.desktop.kde.enable = mkEnableOption "Wether to activate the kde desktop environment.";

  config = mkIf cfg.desktop.kde.enable {
    # KDE-specific system packages.
    environment.systemPackages = with pkgs; [
      # Themes.
      adapta-kde-theme
      # File utils.
      krename
      # Google drive integration
      kdePackages.kaccounts-integration
      kdePackages.kaccounts-providers
      kdePackages.kio-gdrive
      # Media players.
      dragon
      # Image utils.
      kcolorchooser
      kolourpaint
      # CD burning.
      kdePackages.k3b
      # Social.
      kaddressbook
      kmail
      kontact
      konversation
      # Encryption.
      kgpg
      kleopatra
      kcharselect
      # Integrations.
      kdePackages.kdeconnect-kde
      kdePackages.dolphin-plugins
      plasma-browser-integration
      # Office tools.
      kcalc
      # Terminals.
      yakuake
      # System tools.
      ksystemlog
      sddm-kcm
      networkmanagerapplet
    ] ++ (if cfg.profiles.network.bluetooth.enable then [
      bluedevil
    ] else [ 
    ] ++ (if cfg.flatpak.enable then [
      # Flatpak-compatible package manager
      kdePackages.discover
    ] else []));
    # Enable xdg desktop portals.
    xdg.portal = {
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-kde ];
    };
    # KDE-specific services.
    services = {
      desktopManager.plasma6.enable = true;
      displayManager = {
        sddm = {
          enable = true;
          autoNumlock = true;
        };
        defaultSession = "plasma";
      };
      libinput.enable = true;
    };
    # KDE-specific env vars.
    environment.variables = {
      KDEDIRS = "$HOME/kde5/inst:$KDEDIRS";
      KDEHOME = "$HOME/.config/kde5";
      KDETMP = "$HOME/.cache/kde";
      KDEVARTMP = "$HOME/.cache/kde/var";
      KDESYCOCA = "$HOME/.cache/ksycoca";
    };
    # Enable network-manager and open ports needed by KDE connect
    networking = {
      networkmanager.enable = true;
      # Kde connect
      # https://community.kde.org/KDEConnect#I_have_two_devices_running_KDE_Connect_on_the_same_network.2C_but_they_can.27t_see_each_other
      firewall.allowedTCPPortRanges = [{ from = 1714; to = 1764; }];
      firewall.allowedUDPPortRanges = [{ from = 1714; to = 1764; }];
    };
  };

}

{ cfg, lib, pkgs }:

with lib;

{
  options.marti.roles.desktop.xfce.enable = mkEnableOption "Wether to activate the xfce desktop environment.";

  config = mkIf cfg.desktop.xfce.enable {
    environment.systemPackages = with pkgs; with channels; [
      # xfce components.
      xfce.xfce4panel
      xfce.xfce4settings
      xfce.xfce4session
      xfce.xfce4taskmanager
      xfce.xfce4volumed
      xfce.xfwm4themes
      # xfce plugins.
      xfce.xfce4_verve_plugin
      xfce.xfce4_whiskermenu_plugin
      xfce.xfce4_dockbarx_plugin
      # external applets.
      dockbarx
      networkmanagerapplet
    ];
    services = {
      xserver = {
        desktopManager = {
          xfce.enable = true;
        };
        displayManager = {
          lightdm.enable = true;
          defaultSession = "xfce";
        };
      };
      libinput.enable = true;
    };
    networking.networkmanager.enable = true;
  };

}

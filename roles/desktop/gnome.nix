{ cfg, lib, pkgs }:

with lib;

{

  options.marti.roles.desktop.gnome.enable = mkEnableOption "Wether to activate the gnome desktop environment.";

  config = mkIf cfg.desktop.gnome.enable {
    # TODO: implement gnome desktop role
  };

}

# Marti's shared function library.

# import nixpkgs lib
{ lib }:
let

  inherit (builtins) length head all isList isAttrs concatLists;

  inherit (lib) unique last zipAttrsWith naturalSort;

in
rec {

  /*

    Recursively merge a list of values, merging attrsets recursively,
    concatenating lists and outputting the last value in the other cases.

    Example:

    x = {
    a = {
    xa1 = true;
    };
    b = [ "xb1" "xb2" ];
    c = "xc";
    };

    y = {
    a = {
    ya1 = true;
    };
    b = [ "yb1" "yb2" ];
    c = "yc";
    };

    a = [ "a1" "a2" "z" ];

    b = [ "b1" "b2" "z" ];

    recursive_merge [ x y ]
    => {
    a = { xa1 = true; ya1 = true; }; # merged attrsets
    b = [ "xb1" "xb2" "yb1" "yb2" ]; # concatenated lists
    c = "yc"; # last value
    }

    recursive_merge [ y x ]
    => {
    a = { xa1 = true; ya1 = true; }; # merged attrsets
    b = [ "xb1" "xb2" "yb1" "yb2" ]; # concatenated lists
    c = "xc"; # last value
    };

    recursive_merge [ a b ]
    => [ "a1" "a2" "b1" "b2" "z" ]; # concatenated lists

    recursive_merge [ x a ]
    => [ "a1" "a2" "z" ]; # last value

  */
  recursive_merge = values:
    # if the list is empty
    if length values == 0
    # fail with an error
    then abort "trying to recursively merge an empty list, nothing to merge"
    # if there is only one value
    else if length values == 1
    # return it
    then head values
    # if all the values are lists
    else if all isList values
    # concatenate them, remove duplicates and sort, and return the new list
    then naturalSort (unique (concatLists values))
    # if all the values are attrsets
    else if all isAttrs values
    # recursively merge them
    then zipAttrsWith (name: values: recursive_merge values) values
    # if the values are neither lists nor attrsets, return the last one
    else last values;
}

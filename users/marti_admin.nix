{ pkgs, dotfiles }:

let
  name = "marti_admin";
  fullName = "Administrador Marti";
  uid = 1984;
  home = "/home/${name}";
  email = "marti.serra@protonmail.com";
in
{
  # create /var/run/${uid} and /nix/var/nix/profiles/per-user/${name}
  system.activationScripts."user-${name}" = ''
    ${pkgs.coreutils}/bin/mkdir -m 0700 -p /run/user/${toString uid} /nix/var/nix/profiles/per-user/${name}
    ${pkgs.coreutils}/bin/chown ${name}:users /run/user/${toString uid} /nix/var/nix/profiles/per-user/${name}
  '';

  users.users.${name} = {
    inherit uid home;
    description = fullName;
    isNormalUser = true;
    createHome = true;
    extraGroups = [ "wheel" "nixbld" ];
    # Allow local access with default password until password change, useful for vms.
    # Generate with 'mkpasswd -m sha-512'
    initialHashedPassword = "$6$swwHusbXlqoR8agA$L88JPDtGZbnUOJeYfS9kuqWP1YwbPZQXHcHWfjEjyuSZnBVzrFc/qaBvFBlyHb7gTlrXb2ycc.yxbutX2appF.";
    openssh.authorizedKeys.keys = [
      # marti casa
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAK7PSl4xpvZgAD2HstjuuyH0+5hCMV74a/oW7jgrB+a xvapx@xvapx"
    ];
    packages = with pkgs; [
      git
      neovim
      tmux
    ];
  };

  home-manager.users.${name} = {
    home = {
      stateVersion = "22.05";
      file.".config/git/config".text = dotfiles.marti.git.config {
        inherit email;
        name = fullName;
      };
      file.".config/nvim/init.vim".text = dotfiles.marti.neovim.init {
        inherit home;
      };
      file.".tmux.conf".text = dotfiles.marti.tmux.conf;
      file.".inputrc".text = dotfiles.marti.readline.inputrc;
      file.".bash_profile".text = dotfiles.marti.bash.profile;
      file.".bashrc".text = dotfiles.marti.bash.rc {
        tmux = true;
        direnv = true;
        fasd = true;
        git = true;
        ssh-agent = true;
      };
      file.".bash_aliases".text = dotfiles.marti.bash.aliases {
        git = true;
        fasd = true;
        virtualization = true;
      };
      file.".bash_locale".text = dotfiles.marti.bash.locale;
      file.".cache/kde/.keep".text = "";
      sessionVariables = {
        XDG_CACHE_HOME = "${home}/.cache";
        XDG_CONFIG_HOME = "${home}/.config/";
        XDG_DATA_HOME = "${home}/.local/share";
        XDG_STATE_HOME = "${home}/.local/state";
        XDG_RUNTIME_DIR = "/run/user/${toString uid}";
      };
    };
  };

}

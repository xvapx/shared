{ cfg, lib, pkgs }:

with lib;
with builtins;

let

  # Helper function to generate nixos filesystems entries.
  processed_disks = disks: rec {

    # Generate a list of boot devices, if it's empty use "nodev".
    boot_disks = filterAttrs (n: v: (hasAttr "boot" v && v.boot == true)) disks;
    boot_devices = attrValues (mapAttrs (n: v: (v.device)) boot_disks);
    boot_device = if boot_devices == [ ] then "nodev" else "";

    # Generate a list of disks to spin-down.
    spin_down_disks = filterAttrs (n: v: (hasAttr "spin-down" v && v.spin-down == true)) disks;
    spin_down_devices = attrValues (mapAttrs (n: v: (v.device)) spin_down_disks);

    # Generate a list of disks in which to disable write cache.
    disable_write_cache_disks = filterAttrs (n: v:
      (hasAttr "disable_write_cache" v && v.disable_write_cache == true)) disks;
    disable_write_cache_devices = attrValues (mapAttrs (n: v: (v.device)) disable_write_cache_disks);

    # Generate a list of disks to monitor with smartd and their options.
    monitor_disks = filterAttrs (n: v: (hasAttr "smartd" v && v.smartd == true)) disks;
    monitor_devices = attrValues (mapAttrs
      (n: v: ({
        device = v.device;
        options = (if hasAttr "smartd-options" v then v.smartd-options else "");
      }))
      monitor_disks);

    # Generate an attrset of only the disks with declared partitions.
    disks_with_partitions = filterAttrs (
      disk_name: disk_value: hasAttr "partitions" disk_value ) disks;

    # Generate a list with all the partitions:
    partitions_list =
      # Collect into a single list the values
      flatten (attrValues (
        # of deconstructing the attrset of each disk,
        mapAttrs
          (dn: dv:
            # deconstructing attrset of each partition,
            attrValues (mapAttrs
              (pn: pv:
                # and taking the value of each partition
                (pv)
                # from each disk
              ) dv.partitions)
            # from the disks_with_partitions attrset.
          ) disks_with_partitions));

    # Generate a nixos filesystem entry from a mountable partition:
    generate_filesystem_entry = partition:
      # Generate an attrset
      nameValuePair
        # with the mountpoint as the name,
        (partition.mountpoint)
        # and this attrset as the value:
        ({
          fsType = partition.filesystem;
          device = (
            # If the partition specifies a device, use it.
            if partition ? "device" then partition.device
            # If the partition has a label, generate the device attribute using it.
            else if partition ? "label" then "/dev/disk/by-label/${partition.label}"
            else throw "A partition must have a 'label' or 'device' attribute to be mounted."
          );
          options = (if partition ? "options" then partition.options else [ "defaults" ]);
        });

    # Generate a nixos swap entry from a swap partition:
    generate_swap_entry = partition: {
      device = (
        # If the partition specifies a device, use it.
        if partition ? "device" then partition.device
        # If the partition has a label, generate the device attribute using it.
        else if partition ? "label" then "/dev/disk/by-label/${partition.label}"
        else throw "A partition must have a 'label' or 'device' attribute to used as swap."
      );
      options = (if partition ? "options" then partition.options else [ "defaults" ]);
      encrypted = (if partition ? "encrypted" then partition.encrypted else { enable = false; });
    };

    # Generate a list of the partitions with mountpoint:
    mountable_partitions_list =
      # For each partition, generate a filesystem entry
      map (p: generate_filesystem_entry p)
        # if it has a mountpoint, from the partitions_list.
        (partition (p: p ? "mountpoint") partitions_list).right;

    # Generate a list of the partitions with filesystem = "swap":
    swap_partitions_list =
      # For each partition, generate a swap entry
      map (p: generate_swap_entry p)
        # If it has swap as filesystem.
        (partition (p: (p ? "filesystem" && p.filesystem == "swap")) partitions_list).right;

    # Generate an attrset with all the mountable partitions.
    mountable_partitions = listToAttrs mountable_partitions_list;

  };

in

{

  options.marti.profiles.filesystems = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Wether to enable marti's shared filesystem profiles.";
    };
    disks = mkOption {
      type = types.attrsOf types.anything;
      default = { };
      description = ''
        Attrset with all the disks in this machine.
        For a disk to be used, it must have a "partitions" attribute.
        For a partition to be mounted, it must have a "mountpoint", either "label" or "device", and filesystem.
        For a partition to be used as swap, it must have filesystem = "swap".
      '';
      example = {
        system = {
          boot = true;
          smardt = true;
          smartd-options = "-d sat";
          partitions = {
            boot = {
              label = "boot";
              filesystem = "ext2";
              mountpoint = "/boot";
            };
            nixos = {
              label = "nixos";
              filesystem = "ext4";
              mountpoint = "/";
            };
            swap = {
              device = "/dev/sdb1";
              filesystem = "swap";
            };
          };
        };
      };
    };
    supported = mkOption {
      type = types.listOf types.str;
      default = [ "btrfs" "xfs" ];
      description = "List of supported filesystems (In adition to ext and fat).";
    };
    mdadm.conf = mkOption {
      type = types.lines;
      default = "";
      description = "mdadm configuration.";
    };
    boot = {
      timeout = mkOption {
        type = types.int;
        default = 3;
        description = "Time in seconds for the boot loader to auto-select the default boot option.";
      };
      efi = {
        support = mkEnableOption "Wether to enable boot support for EFI.";
        # To use efi NVRAM this must be true, otherwise it must be false.
        modifiable = mkEnableOption "Wether the installation process is allowed to modify EFI boot variables.";
        # To use efi NVRAM this must be false, otherwise it must be true.
        removable = mkEnableOption "Wether grub will be installed with --removable option.";
        mountPoint = mkOption {
          type = types.path;
          default = "/boot";
          description = "Efi partition mount point.";
        };
      };
      preLVMCommands = mkOption {
        type = types.lines;
        default = "";
        description = "Shell commands to run before LVM discovery.";
      };
      luks = {
        secrets = mkOption {
          type = types.attrsOf types.anything;
          default = { };
          example = {
            "/boot.bin" = /etc/secrets/keys/boot.bin;
          };
          description = "Keys for unlocking encrypted partitions.";
        };
        reusePassphrases = mkOption {
          type = types.bool;
          default = true;
          description = "Wether luks should try the first succesful passphrase for subsequent devices.";
        };
        devices = mkOption {
          type = types.attrsOf types.anything;
          default = { };
          description = "Attrset of luks devices and their keys locations in the initial ramdisk.";
          example = {
            boot = {
              device = "/dev/disk/by-uuid/...";
              preLVM = false;
              keyFile = "/boot.bin";
            };
          };
        };
      };
      entries = {
        halt = mkOption {
          type = types.bool;
          default = true;
          description = "Wether to have a halt option in the bootloader.";
        };
        reboot = mkOption {
          type = types.bool;
          default = true;
          description = "Wether to have a reboot option in the bootloader.";
        };
        extra = mkOption {
          type = types.lines;
          default = "";
          description = "Extra entries to display in the bootloader.";
        };
        name = mkOption {
          type = types.str;
          default = "";
          description = "Grub entry name instead of default.";
        };
      };
    };
    smartd = {
      defaults = {
        monitored = mkOption {
          type = types.str;
          # -a Enables a lot of checks and reports
          # -o on Enables SMART automatic offline testing on smartd startup
          # -S on Enables attribute autosave on smartd startup
          # -s (O/../.././(00|06|12|18)|S/../.././03|L/../../1/04)
          # Runs self-tests:
          # Offline immediate test daily at 00:00, 06:00, 12:00 and 18:00
          # Short self-test daily at 04:00
          # Long self-test weekly on Mondays at 04:00, skipping the short daily self-test
          # -W 5,40,50 Tracks temperature changes of >= 5º, logs temperatures of >= 40º
          # and warns of temperatures >= 50º
          default = "-a -o on -S on -s (O/../.././(00|06|12|18)|S/../.././04|L/../../1/04) -W 5,40,50";
          description = "smartd settings for explicitliy monitored disks, see man 5 smartd.conf for options.";
        };
      };
    };
    spin-down = {
      s-value = mkOption {
        type = types.int;
        default = 120;
        description = "-S value for hdparm to spin down the disk. See man hdparm.";
      };
    };
    kernel = {
      modules = mkOption {
        type = types.listOf types.str;
        default = [
          "ahci"
          "ohci_pci"
          "ehci_pci"
          "ata_piix"
          "pata_atiixp"
          "pata_jmicron"
          "firewire_ohci"
          "usbhid"
          "usb_storage"
          "sd_mod"
          "sr_mod"
          # raid support
          "dm-raid" "dm_integrity" "raid0" "raid1" "raid456"
        ];
        description = "Kernel modules available during boot.";
      };
      sysctl = mkOption {
        type = types.attrsOf types.anything;
        default = { };
        description = "Set of runtime parameters of the linux kernel, see man 8 sysctl for more info.";
      };
    };
  };

  config = mkIf cfg.filesystems.enable {

    boot = {
      swraid = {
        enable = true;
      };
      loader = {
        grub = {
          enable = true;
          # Enable grub support for encrypted partitions.
          enableCryptodisk = true;
          # Allow grub to detect other OSes.
          useOSProber = true;
          efiSupport = cfg.filesystems.boot.efi.support;
          efiInstallAsRemovable = cfg.filesystems.boot.efi.removable;
          extraEntries = (if cfg.filesystems.boot.entries.halt then ''
            menuentry "Poweroff" {
              halt
            }
          '' else "") + (if cfg.filesystems.boot.entries.reboot then ''
            menuentry "Reboot" {
              reboot
            }
          '' else "") + cfg.filesystems.boot.entries.extra;
          configurationName = cfg.filesystems.boot.entries.name;
          device = (processed_disks cfg.filesystems.disks).boot_device;
          devices = (processed_disks cfg.filesystems.disks).boot_devices;
        };
        efi = {
          canTouchEfiVariables = cfg.filesystems.boot.efi.modifiable;
          efiSysMountPoint = cfg.filesystems.boot.efi.mountPoint;
        };
        # Grub timeout, in seconds.
        timeout = cfg.filesystems.boot.timeout;
      };
      initrd = with cfg.filesystems; {
        services = {
          lvm.enable = true;
        };
        availableKernelModules = kernel.modules;
        supportedFilesystems = supported;
        preLVMCommands = boot.preLVMCommands;
        secrets = boot.luks.secrets;
        luks = {
          reusePassphrases = boot.luks.reusePassphrases;
          devices = boot.luks.devices;
        };
      };
      supportedFilesystems = cfg.filesystems.supported;
      kernel.sysctl = {
        # Enable all sysreq functions.
        "kernel.sysrq" = 1;
        # Set high inotify limit.
        "fs.inotify.max_user_watches" = 524288;
      } // cfg.filesystems.kernel.sysctl;
    };
    # Monitor disks health with smartd.
    services.smartd =
      if (processed_disks cfg.filesystems.disks).monitor_devices != [ ] then {
        enable = true;
        defaults.monitored = cfg.filesystems.smartd.defaults.monitored;
        devices = (processed_disks cfg.filesystems.disks).monitor_devices;
      } else {
        enable = false;
      };
    # spin down disks if needed.
    powerManagement.powerUpCommands = with cfg.filesystems; (
      if (processed_disks cfg.filesystems.disks).spin_down_devices != [ ] then ''
        ${pkgs.hdparm}/sbin/hdparm -S ${toString spin-down.s-value} -B 127 ${concatStringsSep " " (processed_disks cfg.filesystems.disks).spin_down_devices}
      '' else ""
    ) + (if (processed_disks cfg.filesystems.disks).disable_write_cache_devices != [ ] then ''
      ${pkgs.hdparm}/sbin/hdparm -W 0 ${concatStringsSep " " (processed_disks cfg.filesystems.disks).disable_write_cache_devices}
    '' else "");

    # Configure mdadm.
    environment.etc."mdadm.conf".text = cfg.filesystems.mdadm.conf;

    fileSystems = (processed_disks cfg.filesystems.disks).mountable_partitions;
    swapDevices = (processed_disks cfg.filesystems.disks).swap_partitions_list;
  };

}

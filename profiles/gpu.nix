{ cfg, lib, pkgs }:

with lib;

{

  options.marti.profiles.gpu = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Wether to enable marti's shared gpu profiles.";
    };
    vendor = mkOption {
      type = types.enum [ "unknown" "amd" "intel" "matrox" "nvidia" ];
      default = "unknown";
      description = "cpu vendor";
    };
    graphics = {
      enable = mkEnableOption "Wether to enable wayland/opengl on this system.";
    };
    drivers = mkOption {
      type = types.listOf types.str;
      default = [ "modesetting" ];
      description = "gpu drivers to use in this system.";
    };
    xrandrHeads = mkOption {
      type = types.listOf (types.attrsOf types.anything);
      default = [ ];
      description = "xrandrHeads configuration for this machine.";
    };
  };

  config = mkIf cfg.gpu.enable {
    # Enable wayland/opengl if needed, and install related packages.
    hardware.graphics = {
      enable = cfg.gpu.graphics.enable;
      extraPackages = with pkgs; if (cfg.gpu.vendor == "amd" || cfg.gpu.vendor == "nvidia") then [
        libvdpau-va-gl
        vaapiVdpau
        amdvlk
      ] else if cfg.gpu.vendor == "intel" then [
        intel-media-driver
        libvdpau-va-gl
        vaapiIntel
        vaapiVdpau
      ] else [ ];
      extraPackages32 = with pkgs; if cfg.gpu.vendor == "amd" then [
        driversi686Linux.amdvlk
      ] else [ ];
    };
    # Enable gpu drivers
    services.xserver = {
      videoDrivers = if cfg.gpu.drivers != [ ] then cfg.gpu.drivers else
        [ "modesetting" ];
      xrandrHeads = cfg.gpu.xrandrHeads;
      # symlink the X configuration to /etc/X11/xorg.conf
      exportConfiguration = true;
    };
    # Enable vendor-specific tools
    environment.systemPackages = with pkgs; if cfg.gpu.vendor == "amd" then [
      radeontop
    ] else if cfg.gpu.vendor == "intel" then [
      intel-gpu-tools
    ] else [ ];
  };

}

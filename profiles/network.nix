{ cfg, lib, pkgs }:

with lib;

{

  options.marti.profiles.network = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Wether to enable marti's shared network profiles.";
    };
    bluetooth.enable = mkEnableOption "Wether to enable bluetooth.";
    hostName = mkOption {
      type = types.str;
      description = "hostname of this machine.";
    };
    hostId = mkOption {
      type = types.str;
      description = "unique network id of this machine.";
    };
    settings = mkOption {
      type = types.attrsOf types.anything;
      default = {
        hostName = "nixos";
        hostId = null;
        defaultGateway = null;
        nameservers = [ ];
        firewall = {};
      };
      description = "attribute set with nixos networking configuration.";
    };
    interfaces = mkOption {
      type = types.attrsOf types.anything;
      description = "Network interfaces in this machine.";
      example = {
        primary = "enp1s0";
      };
    };
    bonds = mkOption {
      type = types.attrsOf types.anything;
      default = { };
      description = "Network bonds in this machine.";
    };
    bridges = mkOption {
      type = types.attrsOf types.anything;
      default = { };
      description = "Network bridges in this machine.";
    };
    vlans = mkOption {
      type = types.attrsOf types.anything;
      default = { };
      description = "Vlans in this machine.";
    };
  };

  config = mkIf cfg.network.enable {
    # Enable bluetooth if needed
    hardware.bluetooth.enable = cfg.network.bluetooth.enable;
    # Set up networking
    networking = {
      usePredictableInterfaceNames = mkDefault true;
      resolvconf = mkDefault {
        enable = true;
      };
      inherit (cfg.network.settings) hostName hostId defaultGateway nameservers firewall;
      # Iterate over the interfaces
      interfaces = mapAttrs
        (name: value:
          # And generate the value according to the network settings of that interface
          {
            # TODO: ipv6 compatibility
            ipv4.addresses = [{
              address = value.ip;
              inherit (value) prefixLength;
            }];
            useDHCP = if value.ip == "" then true else false;
          }
        )
        cfg.network.interfaces;
      useDHCP = false;
      bonds = cfg.network.bonds;
      bridges = cfg.network.bridges;
      vlans = cfg.network.vlans;
    };
  };

}

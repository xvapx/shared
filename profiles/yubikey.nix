{ cfg, lib, pkgs }:

with lib;

{

  options.marti.profiles.yubikey = {
    enable = mkEnableOption "Wether to enable yubikey software.";
  };

  config = mkIf cfg.yubikey.enable {
    environment.systemPackages = with pkgs; (mkMerge [
      [
        yubikey-manager
        yubioath-flutter
        yubico-piv-tool
        yubikey-manager-qt
      ]
    ]);
    services = {
      pcscd.enable = true;
      udev.packages = [
        pkgs.yubikey-personalization
        pkgs.libu2f-host
      ];
    };
  };

}

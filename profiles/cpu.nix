{ cfg, lib, pkgs }:

with lib;

{

  options.marti.profiles.cpu = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Wether to enable marti's shared cpu profiles.";
    };
    vendor = mkOption {
      type = types.enum [ "unknown" "amd" "intel" ];
      default = "unknown";
      description = "cpu vendor.";
    };
    cores = mkOption {
      type = types.int;
      default = 1;
      description = "number of cores for this cpu.";
    };
  };

  config = mkIf cfg.cpu.enable {
    # Enable cpu microcode updates
    hardware.cpu = {
      amd.updateMicrocode = if cfg.cpu.vendor == "amd" then true else false;
      intel.updateMicrocode = if cfg.cpu.vendor == "intel" then true else false;
    };
    # Allow nix to use all the cores
    nix.settings = {
      max-jobs = cfg.cpu.cores;
      cores = cfg.cpu.cores;
    };
  };

}

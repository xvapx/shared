{ stdenv, lib }:

stdenv.mkDerivation rec {
  name = "ssh-moduli";
  version = "2022";

  # To generate this file (takes 1h+):
  # ssh-keygen -G moduli.tmp -b 4096
  # ssh-keygen -T moduli -f moduli.tmp
  # rm moduli.tmp
  src = ./moduli;

  phases = [ "installPhase" ];

  installPhase = ''
    mkdir -p $out/
    cp ${src} $out/ssh-moduli
  '';

  meta = with lib; {
    description = "Shared ssh-moduli file.";
    license = licenses.mit;
    platforms = platforms.linux;
  };

}


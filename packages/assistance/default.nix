# Marti shared remote assistance package.

{ stdenv, lib, imagemagick, optipng, curl, x11vnc, port ? 2389, lang ? "en" }:

let

  # localized texts
  languages = {
    # catalan
    ca = {
      name = "assistencia";
      comment = "Assistència remota.";
      welcome = "Benvingut a assistència remota.";
      ip = "Si us plau, facilita aquesta adreça IP al tècnic remot:";
      password = "El password aleatori per aquesta sessió és:";
    };
    # spanish
    es = {
      name = "asistencia";
      comment = "Asistencia remota";
      welcome = "Bienvenido a asistencia remota.";
      ip = "Por favor, facilita esta dirección IP al técnico remoto:";
      password = "El password aleatorio para esta sesión es:";
    };
    # english
    en = {
      name = "assistance";
      comment = "Remote assistance.";
      welcome = "Welcome to remote assistance.";
      ip = "Please, give this IP address to the remote technician:";
      password = "The random password for this session is:";
    };
  };

  # helper function to show a localized text
  localText = text: languages."${lang}"."${text}";

in

stdenv.mkDerivation rec {
  name = localText "name";
  version = "2022";

  src = ./.;

  buildInputs = [ curl x11vnc ];

  nativeBuildInputs = [ imagemagick optipng ];

  phases = [ "unpackPhase" "buildPhase" "installPhase" ];

  buildPhase = ''
    cat <<EOF > ${name}
    #!/usr/bin/env bash
    divider="============================================================"
    ip=\$(curl https://ipinfo.io/ip)
    password=\$( < /dev/urandom tr -dc A-Z0-9 | head -c 10)
    clear
    printf "%s\n" \
    "" \
    "\$divider" \
    "${localText "welcome"}" \
    "${localText "ip"}" \
    "\$ip:${toString port}" \
    "${localText "password"}" \
    "\$password" \
    "\$divider"
    x11vnc -q -passwd "\$password" -noxrecord -localhost -display :0 -nevershared -rfbport ${toString port} 2>/dev/null 1>&2
    EOF

    cat <<EOF > ${name}.desktop
    [Desktop Entry]
    Encoding=UTF-8
    Type=Application
    # The version of the desktop entry specification to which this file complies
    Version=1.0
    # The name of the application
    Name=${name}
    # A comment which can/will be used as a tooltip
    Comment=${localText "comment"}
    # The executable of the application, possibly with arguments.
    Exec=${name}
    # Describes whether this application needs to be run in a terminal or not
    Terminal=true
    # The name of the icon that will be used to display this entry
    Icon=${name}
    # Describes the categories in which this entry should be shown
    Categories=Settings;System;Network;RemoteAccess;
    EOF

    # Icons to generate from the svg
    sizes=( 16 22 24 32 48 128 256 )
    for size in ''${sizes[@]}; do
      convert -density ''${size}x''${size} -background transparent logo.svg -resize ''${size}x''${size} -colors 256 logo-''${size}.png
      optipng -o7 -clobber logo-''${size}.png
    done
  '';

  installPhase = ''
    # generate and copy icons
    icon_sizes=( 16 22 24 32 48 128 256 )
    mkdir -p $out/{bin,share/applications}
    for size in ''${icon_sizes[@]}; do
      mkdir -p $out/share/icons/hicolor/''${size}x''${size}/apps
      cp logo-''${size}.png $out/share/icons/hicolor/''${size}x''${size}/apps/${name}.png
    done
    mkdir -p $out/share/icons/hicolor/scalable/apps
    cp logo.svg $out/share/icons/hicolor/scalable/apps/${name}.svg

    # copy binary
    cp ${name} $out/bin/${name}
    chmod +x $out/bin/${name}

    # copy desktop file
    cp ${name}.desktop $out/share/applications/
  '';

  meta = with lib; {
    description = "remote assistance";
    license = licenses.gpl3Plus;
    platforms = platforms.linux;
  };

}
